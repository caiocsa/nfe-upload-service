package br.com.nfe.model;

import lombok.Getter;

@Getter
public enum StatusProcessamento {

	AGUARDANDO_PROCESSAMENTO,
	EM_PROCESSAMENTO,
	PROCESSADA,
	PROCESSADA_COM_ERRO;
}