package br.com.nfe.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class NotaFiscal {

	@ApiModelProperty(value = "Identificador Nota Fiscal", name = "id", dataType = "Long", example = "123")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ApiModelProperty(value = "Número da Nota Fiscal", name = "numero", dataType = "Long", example = "123")
	@NotNull
	@Column(name = "NUMERO")
	private Long numero;

	@ApiModelProperty(value = "Data/Hora registro da Nota Fiscal", name = "dataHoraRegistro", dataType = "Date")
	@NotNull
	@Column(name = "DH_REGISTRO")
	private Date dataHoraRegistro;

	@ApiModelProperty(value = "Nome do Emitente", name = "nomeEmitente", dataType = "String", example = "João")
	@NotNull
	@Column(name = "NOME_EMITENTE")
	private String nomeEmitente;

	@ApiModelProperty(value = "Nome do Destinatario", name = "nomeDestinatario", dataType = "String", example = "João")
	@NotNull
	@Column(name = "NOME_DESTINATARIO")
	private String nomeDestinatario;

	@ApiModelProperty(value = "Valor da Nota Fiscal", name = "valorNota", dataType = "BigDecimal", example = "João")
	@NotNull
	@Column(name = "VALOR_NOTA")
	private String valorNota;

	@Enumerated(EnumType.STRING)
	@Column(name = "STATUS_PROCESSAMENTO")
	private StatusProcessamento statusProcessamento;
}