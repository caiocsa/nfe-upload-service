package br.com.nfe.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MessageResponse {

	private String message;
	private Object errors;
}
