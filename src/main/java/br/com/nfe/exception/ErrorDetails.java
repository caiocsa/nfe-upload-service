package br.com.nfe.exception;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(value = Include.NON_NULL)
public class ErrorDetails {

	@ApiModelProperty(value = "Error message", dataType = "String", required = true)
	private Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());

	@ApiModelProperty(value = "Error status code", dataType = "int", required = true)
	private int status;

	@ApiModelProperty(value = "Error title", dataType = "String", required = true)
	private String error;

	@ApiModelProperty(value = "Exception that was thrown", dataType = "String")
	private String exception;

	@ApiModelProperty(value = "Error message", dataType = "String", required = true)
	private String message;

	@ApiModelProperty(value = "Additional error messages", dataType = "Map<Title,Message>")
	private Object details;
}
