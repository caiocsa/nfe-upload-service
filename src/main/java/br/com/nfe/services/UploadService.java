package br.com.nfe.services;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.nfe.exception.IntegrationException;
import br.com.nfe.model.NotaFiscal;
import br.com.nfe.model.StatusProcessamento;
import br.com.nfe.repository.NotaFiscalRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UploadService {

	private final ArquivoService arquivoService;
	private final NotaFiscalRepository repository;

	public NotaFiscal salvarNotaFiscal(MultipartFile arquivo) throws IntegrationException {
		NotaFiscal nf = arquivoService.convertXmlToPojo(arquivo);
		nf.setStatusProcessamento(StatusProcessamento.AGUARDANDO_PROCESSAMENTO);
		salvarNotaFiscal(nf);
		arquivoService.salvarArquivo(arquivo);
		return nf;
	}

	private NotaFiscal salvarNotaFiscal(NotaFiscal nf) throws IntegrationException {
		try {
			return repository.save(nf);
		} catch (Exception e) {
			throw new IntegrationException("Falha ao salvar a nota fiscal na base de dados.");
		}
	}
}