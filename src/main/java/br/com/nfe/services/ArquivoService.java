package br.com.nfe.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;

import br.com.nfe.exception.IntegrationException;
import br.com.nfe.model.NotaFiscal;

@Component
public class ArquivoService {

	@Value("${diretorio.raiz}")
	private String raiz;

	@Value("${diretorio.input-nf}")
	private String diretorioInput;

	public void salvarArquivo(MultipartFile arquivo) throws IntegrationException {
		try {
			this.salvar(this.diretorioInput, arquivo);
		} catch (Exception e) {
			throw new IntegrationException("Falha ao salvar o arquivo xml em disco.");
		}
	}

	private void salvar(String diretorio, MultipartFile arquivo) throws IOException {
		Path diretorioPath = Paths.get(this.raiz, diretorio);
		Path arquivoPath = diretorioPath.resolve(arquivo.getOriginalFilename());
		Files.createDirectories(diretorioPath);
		arquivo.transferTo(arquivoPath.toFile());
	}

	public NotaFiscal convertXmlToPojo(MultipartFile arquivo) throws IntegrationException {
		if (!arquivo.getOriginalFilename().endsWith(".xml")) {
			throw new IntegrationException("Formato do Arquivo deve ser XML");
		}
		try {
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = docBuilder.parse(arquivo.getInputStream());
			org.w3c.dom.Element varElement = document.getDocumentElement();
			JAXBContext context = JAXBContext.newInstance(NotaFiscal.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			JAXBElement<NotaFiscal> loader = unmarshaller.unmarshal(varElement, NotaFiscal.class);
			return loader.getValue();
		} catch (Exception e) {
			throw new IntegrationException("Falha ao realizar leitura do arquivo xml.");
		}
	}
}