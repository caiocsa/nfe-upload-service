package br.com.nfe.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.nfe.exception.IntegrationException;
import br.com.nfe.services.UploadService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/nfe")
@RequiredArgsConstructor
public class UploadController {

	private final UploadService service;

	@ApiResponses(value = { @ApiResponse(code = 200, message = "Upload realizado com sucesso."),
			@ApiResponse(code = 500, message = "Ocorreu um erro inesperado."), })
	@PostMapping("/upload")
	public ResponseEntity<?> upload(@RequestParam("file") MultipartFile arquivo) throws IntegrationException {
		service.salvarNotaFiscal(arquivo);

		return ResponseEntity.status(HttpStatus.OK)
				.body("O Arquivo foi recepcionado com sucesso e que será processado.");
	}
}